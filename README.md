# tic-tac-toe

A classic game of tic-tac-toe played with chat bot.

### Steps to build and run the project:

- Open the project in VS Code.
- Press (Ctrl + ~ ) to open the terminal
- Clone the git repository by pasting the following link in the terminal
  - git clone https://Skaur9167@bitbucket.org/Skaur9167/tic-tac-toe.git
- Install npm using the following command in the terminal
  - npm install
- Open the `indexTicTacToe.js` and press Ctrl + F5 to run the project
- Open your favorite browser and goto http://localhost:3000/ to open the tic-tac-toe chatbot.

### Screenshot from the project

![snapshot](https://bitbucket.org/Skaur9167/tic-tac-toe/raw/815247051432947d577f8d43e9c8c5031ba7e047/images/snapshot.PNG)