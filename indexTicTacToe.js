import express from 'express';
import bodyParser  from "body-parser";
import TicTacToeGame  from "./TicTacToeGame";

// Create a new express application instance
const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static("www"));

app.get("/users/:uname", (req, res) => {
    res.end("Hello " + req.params.uname);
});

let oGames = {}; //we are looking for a associative array if not there , we create it in the 16th line

//here we have a callback that hits the URL again and again when person hits the button
app.post("/sms", (req, res) =>{
    let sFrom = req.body.From; //sFrom is the person whom I'm communicating with
    if(!oGames.hasOwnProperty(sFrom)){
        oGames[sFrom] = new TicTacToeGame();
    }
    let sMessage = req.body.Body|| req.body.body; //populates the message with actual content
    let aReply = [oGames[sFrom].takeTurn(sMessage)]; //actually make a "move" in the game
    //
    //aReply is expecting an array but takeTurn is returning an array ... now put a square brackets around the 
    //entire line ....this will make entire string returned as array 

    res.setHeader('content-type', 'text/xml'); 
    let sResponse = "<Response>"; //sResponse is just building up the response
    for(let n = 0; n < aReply.length; n++){
        sResponse += "<Message>";
        sResponse += aReply[n];
        sResponse += "</Message>";
    }
    res.end(sResponse + "</Response>");

    if(oGames[sFrom].done()){
        delete oGames[sFrom];
    }
});

var port = process.env.PORT || parseInt(process.argv.pop()) || 3000;

app.listen(port, () => console.log('Example app listening on port ' + port + '!'));
