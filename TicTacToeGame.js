import readlineSync from 'readline-sync';

export default class TicTacToeGame{
    
    constructor(){
        this.array_TicTacToc=[["A1","A2","A3"],["B1","B2","B3"],["C1","C2","C3"]];
        this.bDone = false;
    }
    
   getFirstElement(index){
        switch(index){
            case '0':
                return('A');
            case '1':
                return('B');
            case '2':
                return('C');
        }
    }
    
    getIndexFromFirstElement(character){
        switch(character.toUpperCase()){
            case 'A':
                return(0);
            case 'B':
                return(1);
            case 'C':
                return(2);    
        }
    }
    
    storeRandomInGrid(){
        let row = -1;
        let column = -1;
        
        do {
            row = Math.floor(Math.random() * 3);
            column = Math.floor(Math.random() * 3);
        }while(this.alreadyFilled(row,column));

        this.array_TicTacToc[row][column]= "X";
    }

    alreadyFilled(row,column){
       
        if (this.array_TicTacToc[row][column].startsWith("X") || 
        this.array_TicTacToc[row][column].startsWith("O")){
                return true;
        }
            
        return false;

    }
    
    storeInGrid(firstElement, secondElement){
        
        let row = this.getIndexFromFirstElement(firstElement);
        let column = this.getIndexFromSecondElement(secondElement)
        if (!this.alreadyFilled(row,column)){
            this.array_TicTacToc[row][column] = "O";
            return true;
        }else {
            return false;
        }
    }
    
    whoWins(){
        //check by rows
        for(let i=0;i<3;i++){
            if(this.array_TicTacToc[i][0]==this.array_TicTacToc[i][1] && 
                this.array_TicTacToc[i][0]==this.array_TicTacToc[i][2]){
                    return(this.array_TicTacToc[i][0]);
            }
        }
        // check by columns
        for(let j=0;j<3;j++){
            if(this.array_TicTacToc[0][j]==this.array_TicTacToc[1][j] && 
                this.array_TicTacToc[0][j]==this.array_TicTacToc[2][j]){
                    return(this.array_TicTacToc[0][j]);
            }
        }
        // check diagonals
        if(this.array_TicTacToc[0][0]==this.array_TicTacToc[1][1] && 
            this.array_TicTacToc[0][0]==this.array_TicTacToc[2][2]){
                return (this.array_TicTacToc[0][0]);
            }
        if(this.array_TicTacToc[0][2]==this.array_TicTacToc[1][1] && 
            this.array_TicTacToc[0][2]==this.array_TicTacToc[2][0]){
                return (this.array_TicTacToc[0][2]);
            }
    
        if (this.atleastOneEmpty()){
            return "Continue";
        }
        
        return "tie";
    }
    
    atleastOneEmpty(){
        for(let i=0;i<3;i++){
            for(let j=0;j<3;j++){
                if(this.array_TicTacToc[i][j]!='X' && this.array_TicTacToc[i][j]!='O'){
                    return true;
                }
            }
        }
        return false;
    }
    
    getIndexFromSecondElement(character){
        return (parseInt(character )-1);
    }
    
    printGrid(){
        let output="\n";
        for(let i=0;i<3;i++){
                output+= this.array_TicTacToc[i][0] +"\t|\t"+this.array_TicTacToc[i][1]+"\t|\t"+this.array_TicTacToc[i][2]+ "\n";
                if(i<2)
                output+= "----------\n";
        }
        return output;
    }
    
    getSecondElement(index){
        return (index+1);
    }
    
    takeTurn(userInput){
        var welcome = "Welcome to TIC-TAC-TOE\n\n" + this.printGrid() + "\n\nPlease enter Input in the form of [A-C][1-3]\n";
        var playAgain = "Let's Play Again!!!\n\n" +welcome;
        var regEx = new RegExp('^[a-cA-C][1-3]$', 'g');
        var temp = regEx.exec(userInput);
        if (temp == null){
            return welcome;
           // return "Invalid input."
        }
        //save my turn
        if (!this.storeInGrid(userInput.substr(0,1),userInput.substr(1,1))){
            return "Place already filled. Please choose another spot !!"
        }

        
        if(this.whoWins()=='O'){
            this.bDone=true;
            return( this.printGrid() + "\n \n You Win\n\n"+playAgain);
        }
        
        // computer's turn
        if (this.atleastOneEmpty()){
            this.storeRandomInGrid();
        }

        if(this.whoWins()=='X'){
            this.bDone=true;
            return( this.printGrid() + "\n \n I win \n\n" +playAgain);
        }else if(this.whoWins()=='Continue'){
            this.bDone=false;
            return(this.printGrid()+ "\n \n Your turn now");
        }else{
            this.bDone=true;
            return(  this.printGrid() + "\n \n Its a tie \n\n "+playAgain);
        }
    }

    done(){
        return this.bDone;
    }
}

    
